# postgreSQL practice

Learning PostgreSQL

##  Retrieve the start times of members' bookings
```psql
select starttime 
	from cd.bookings
join cd.members
	on memid = memid
where firstname = 'David' and surname = 'Farrell';
```

## Work out the start times of bookings for tennis courts
```psql
select starttime as start, name
	from 	
			cd.bookings
			join cd.facilities
				 on bookings.facid = facilities.facid
	where
			name like 'Tennis%' and
			starttime >= '2012-09-21' and
			starttime < '2012-09-22'
order by starttime;
```


## Produce a list of all members, along with their recommender
```
select mems.firstname as memfname, mems.surname as memsname,
	   recs.firstname as recfname, recs.surname as recs
	from
		cd.members as mems
		left join cd.members as recs
		on recs.memid = mems.recommendedby
order by (mems.surname, mems.firstname);
```